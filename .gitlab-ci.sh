#!/bin/sh

# Run some general commands so we're up-to-date
composer self-update
composer install --no-interaction --optimize-autoloader

# Setup db
touch database/bit.sqlite
echo "" > database/bit.sqlite

php artisan migrate --database=bit --env=testing
php artisan key:generate

# Start tests
vendor/bin/phpmd app/ text phpmd.xml
vendor/bin/phpcbf --standard=psr2 app/
vendor/bin/phpcs --standard=psr2 --colors app/
vendor/bin/phpunit